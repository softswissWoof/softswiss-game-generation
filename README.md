## Casino Session Generator with PROXY SUPPORT
> This should only be used for educational purposes and is simply to prove the big fraud going on by Softswiss.com, by implementing the exact same methods as used on any of below noted casino's ** frontend **.

These snippets make it possible to easily generate demo & real-money game sessions on DAMA N.V. Casino's (centrifuge).

[Click to generate a session (soon off or might be off at time of trying)](https://pix-api.pointdns.rest/retrieveDemoURL?gameID=1x2gaming:BarbarianGold&origin_target=bets.io)

[Click to start real money session on Bets.io, proxied using this snippets](http://vps-70325c4a.vps.ovh.net/api/bets.io_cookied/games/pragmaticexternal/TheDogHouse/29876)

![Proxied Realmoney Session](https://i.imgur.com/DAtU7uh.png)

*Proxied Real-money Gamesession*


## What is this
These games can and are being edited, so you can do yourself aswell if you wish to start your own casino business.

Right now in these snippets is support for launching session & gamelist scraping:
- [Bitstarz.com](https://www.bitstarz.com/)  (best for real money game sessions)
- [Arlekincasino.com](https://www.arlekincasino.com/)
- [Duxcasino.com](https://www.duxcasino.com)
- [Bets.io](https://bets.io) (best for demo game sessions)

But, there are hundreds casino's using softswiss gamelist format.
To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## Setup 
Setup laravel base and add game-importer and so on together into your routes/api.php.

Insert .sql file.

The proxy should be setup on external box. The game session creator will first try to start session locally if fails to create session (mostly due to geo-locks based on I.P. of your server), it will send a proxy support. 

Proxy files is to be installed on external box, place the included 2 proxy scripts to /App/Helpers and register this in your /App/Providers/AppServiceProvider.php (will upload this).

## Gamelist Scrape & Import
However you can get nice info's that you can use on your copied/grey games like the tournament information on pragmaticplay etc etc.

![Gamelist Import](https://i.imgur.com/0shHFTj.png)

*Scrape softswiss formatted gamelist and import to local database*

After added both the game-importer & session-creator to your routes/api.php, you use following queries:

In below example query, script will use the 'offline' stored gamelist.json (right now set to some gitlabs, you should store these yourself)

```
## yoururl.com/api/game-importer?import=1&origin_target=bets.io&clean=1&origin_proxied=1
```

**Parameters**:
    - import=1  | _Toggle to 0 or remove completely from query to just see the result (dry-run'ish)_
    - origin_target={casinoID}  | _Basically the vector where to retrieve gamelists and create sessions, you should probably setup around 10        casino's so you got access to all games and reliable._
    - clean=1  | _Clean means it will delete the previous records in your local database for the specific origin_target, it will not remove/clean games from other origin_targets, if you set this 0 you have a big chance on tons and tons of duplicates._
    - raw_list_output=0  | _This will instead return the raw gamelist as it is scraped, instead of the transformed gamelist array._
    - origin_proxied=1  | _Use proxy / external server to retrieve LIVE gamelist from specific casino, make sure this server is in a preferential geo-location (germany)_


## Casino Game Session Generator
NOTE: the real money sessions really only can be used and should be used with your own gameserver (taking their fraud games and basically do same as them) as these gamesession url's are redirected & in most cases not a real valid money session but a demo session.

**If you are going to use real-money sessions, you need to use specific game_ids at end of URL.** They are imported to the _internal-realmoneylink_ database field. 
Not all games support all currencies and not all casino's support all currencies, so they are specified when taking the gamelistings.
You simply change the digits on the end of URL to the one said for the currency you wish to generate game.

In below example the snippet will search in your local database for game ID and the origin_target. The snippet will always first try and launch local

Softswiss doing the big fraud makes it so that all games are wrapped in their fraudulant casino's & centrifuges on their external page, this page does include the session url's.

However you can get nice info's that you can use on your copied/grey games like the tournament information on pragmaticplay etc etc.

```php
## yoururl.com/api/session-generator?gameID=1x2gaming:BarbarianGold&origin_target=bets.io
```
**Parameters:**
    - gameID=1x2gaming:BarbarianGold   | _your local game id session stored in local database (done by importer)_
    - origin_target=bets.io  | The casino/vector to generate the game at, the proxy also shows example of a cookied request._
    - redirect=to_origin_wrapper | 'to_game_url', 'to_origin_wrapper', 'to_proxied_wrapper'. This will redirect immediately after succesfull session generation, however do not put this snippets to public, however it can be handy testing yourself or to run grey-games. This is unset by default. _
    - load_content=1  | _This will import the source of game to index.php_

![Session generation](https://i.imgur.com/RP4373v.png)*Session Generator*


## Proxy 
Setup laravel on external box, place ProxyHelper.php & ProxyHelperFacade.php /App/Helpers/, create this directory if you need to.

My suggestion is if you are to use this for Softswiss game generation is to pick Germany based VPS/servers as Softswiss hosts mainly on Hetzner.de which is german based and thus seems more lenient.

After go to the file: `App\Providers\AppServiceProvider.php` and in the method `register()` adds the facade:
```php
$this->app->bind('ProxyHelper', function($app) {
    return new ProxyHelper();
});
```
Remember that you need to import the class (not the one called facade) in `App\Providers\AppServiceProvider.php`

```php
use App\Helpers\ProxyHelper; //or your path
```

Proxy routes:

```php
    Route::match(['get', 'post', 'head', 'patch', 'put', 'delete'] , 'proxy/{slug}', function(Request $request){

        // To redirect the request to a different host, the first parameter will be the host.
        // the second, will be the current path that we want to ignore, it must be the url of the controller (api/proxy)
        //so we're telling you that the new url will be:
        // (host) http://my.server2.test + (deleted)[api/proxy] + ({slug}) /api/avatar/color
        return ProxyHelperFacade::CreateProxy($request)->toHost('http://my.server2.test','api/proxy');
        
        //other way is to tell him the url directly.
        return ProxyHelperFacade::CreateProxy($request)->toUrl('http://my.server2.test/api/avatar/color');
        
        // this second way will no longer be dynamic.
        

    })->where('slug', '([A-Za-z0-9\-\/]+)');
```



Example of a cookie route on bets.io, you should make a helper to keep the cookie 'alive':


```php
    Route::match(['get', 'post', 'head', 'patch', 'put', 'delete'] , 'bets.io_cookied/{slug}', function(Request $request){
        $cookie = 'referral_params=eJwrSk0szs+zTU/MTY0vSi0uKcpMLklNic/Mi0/OL80rKaoEAOQvDaE=; dateamlutsk-_zldp=M6KbIcofZ5OdbzCklHE/wT4m8vct0Wfje3KHtA0uoRoY8NE801Jy2Psphbw8i4k+WGzG+PDOVsw=; dateamlutsk-_zldt=7698a211-3f3c-4241-b261-240e437d0678-0; locale=ImVuIg$
        return ProxyHelperFacade::CreateProxy($request)
                // add a header before sending the request
                ->withHeaders(['cookie' => $cookie])
                // add a Bearer token (this is useful for the client not to have the token, and from the intermediary proxy we add it.
                //Maintain the query of the url.
                ->preserveQuery(true)
                ->toHost('https://api.bets.io','api/bets.io_cookied');

    })->where('slug', '([A-Za-z0-9\-\/]+)');
```



## Backoffice Logins
Can be used to further work expanding the free use of these games.

```shell


1X2 GAMING

URL: https://assets.1x2network.com/login.html Username: am@blueoceangaming.com Password: 1x2Blueocean!

AMATIC

URL: https://drive.slotegrator.com/s/XcbAIjBKs26q5ad

AMIGO GAMING

URL: https://amigogaming.com/client-zone Username: sara.turk@blueoceangaming.com Password: Saraamigo2022!

ASIA GAMING LIVE

URL: ftp://ftp.asia-gaming.net Username: agedit Password: h79as56q

BETGAMES

URL: https://wiki.betgames.tv/index.php?title=Marketing_Material Username: Marketing Password: marketing123

BETSOFT

URL: http://art.betsoftgaming.com/ Username: Mirage.Sara Password: iuEff7

BF GAMING

URL: https://clientarea.bfgames.com/ Username: igamingplatform Password: Cbsxt0gC&dQBkshV83ZK7ui7

BOOONGO

URL: https://drive.google.com/drive/folders/0B3tDaeB8e8xpWU1PT3VBOFBTWkE

URL: https://drive.google.com/drive/folders/1feE3xT5z_nltPMbj29w2mq-dZsik_SmF

BOOMING GAMES

URL: https://docs.google.com/spreadsheets/d/1mZrrvgiRULdvNo2gAPwzZ1m6kLaK5GePkV9Pqp8ggeo/edit?ts=5971de47&actionButton=1#gid=960031242

BETIXON

URL: https://ln.sync.com/dl/6ff30ba70#zhu5ugeg-jyxpk7gm-45h3btxe-9j54tj2q Password: SuperGames

BLUEPRINT

URL: portal.blueprintgaming.com Username: iGP Password: O)&1mMj71TJierNd%!5)2cjg

CASIMI

URL: https://storage.casimi.cz/index.php/s/Zp4CaHkyTLAOKKK URL: https://promo.casimi.cz/blueocean-2022-04-05.pdf

CT GAMING

URL: https://www.ctgaming-interactive.com/?action=active Username: notifications@blueoceangaming.com Password: CTgaming2022!

EBET

URL: https://www.ebet.com/

EGT INTERACTIVE

URL: https://client-area.egt-interactive.com Username: BlueOceanGaming Password: NarXjmx&98Vh#CECs#fa$pk7

ELK

URL: https://www.elk-studios.com/promo-package/

ELYSIUM (EX MAVERICK)

URL: https://drive.google.com/drive/folders/16ChBcm3CwmIe2GlsDnvLKV4MuuCwbKIt

ENDORPHINA

URL: https://endorphina.com/ Username: sara.turk@blueoceangaming.com Password: 919176

ESA GAMING

URL: https://www.esagaming.com/games URL: https://docs.google.com/spreadsheets/d/1N-Fv5IMn8CNcj4_UX8sQ8SACpc6uBkXDsZLQVMxnDF8/edit#gid=0

ESPRESSO GAMES

URL: https://www.dropbox.com/sh/juyhaznmie4he79/AACTqtwYBlzCIvA4CZK8SEroa?dl=0&preview=1.+BlueOcean+Gaming+Game+List+20220214.xlsx URL: https://www.dropbox.com/sh/ci6v4dmj4jajth2/AADtCUvZOPLU8pwHf2iixHWTa?dl=0 URL: https://www.dropbox.com/sh/8mbhuzfvg02qn3w/AAAPcbMmhBFHTttaRLNk3XjHa?dl=0

EURASIAN GAMING

URL: https://www.eagaming.com/en/user/login Username: BlueOcean Password: BlueOcean??

EVOLUTION GAMING

URL: https://clientarea.evolution.com/ Username: kristina.drnovscek@blueoceangaming.com Password: K9Td69IopvZ6sDD23

EVOPLAY

URL: https://cloud.playevoplay.com/ Username: iGP Password: 2RL2Dw%ktF[wpiw8

EZUGI

URL: https://ezugi.com Username: dejan.jovic@blueoceangaming.com Password: ZSMjCuCOnucBfABNAWn64@dz

URL: https://www.dropbox.com/sh/19clvh4cmo4izwz/AABhTeihIiZGDedBU-8b3Zqta?dl=0

FAZI

URL: https://www.dropbox.com/sh/zvcw4e5k0zx5w0f/AABze2lMHF_YjsYyejHzePHra?dl=0

FUGASO GAMING

URL: http://fugaso.com/client-area Username: sara.turk@blueoceangaming.com Password: 9SyE3PS73P

FELIX GAMING

URL: https://promo.felixgaming.com Username: promo@felixgaming.com Password: w&0XdNW)F}O]

FELT GAMING

URL:https://www.dropbox.com/sh/bzk58rabhmmbfe5/AAAAeWrgJCtDkPIKbv8bGmI0a?dl=0

G GAMES

URL: https://airtable.com/shrovzQKRL5oDALxZ/tbl6JEq9lpQ7fp0tJ

GAMING CORPS

URL: https://www.dropbox.com/sh/icv36jkpklhyify/AACZUaYaZKL1ouaEoGxhPZTHa?dl=0

GAMZIX

URL: https://drive.google.com/drive/u/1/folders/1O8QbABcGZaFmvCFqYDbG_gGu0MTXhZd

GAMEART

URL: www.gameart.net/client-area Username: bog.area Password: areaBog987

GANAPATI

URL: https://www.dropbox.com/sh/tlg9gusx9g7ftuj/AAD02AA98Nbe9bZVOCQXAXB3a?dl=0

URL: https://docs.google.com/spreadsheets/d/1nCVr7NdMzcgriK8O64H300YcsS4qQ4cRGZvya5ZBTfI/edit#gid=0

GOLDEN RACE

URL: https://clientzone.goldenrace.com/ Password: GoldenClient

HOLLYWOOD TV

URL: https://www.dropbox.com/s/m5zy6idex142f2b/CasinoTV%20-%204%20Games%20Parts.zip?dl=0

HABANERO

Will, [23-04-2022 10:07] URL: http://client.habanerosystems.com/ & https://haba88.com/go/logopack (logos pack) Username: client@habanerosystems.com Password: habaclient

HACKSAW GAMING

URL: https://www.dropbox.com/sh/cyr32tz6ksa9kjw/AAB4OjONQ0uIiAx4RmQTMRLTa?dl=0

CALETA/INDI

URL: https://www.dropbox.com/sh/a0cmyyjjwj3rhfs/AACO6aYciQbzHBrcMSFLRJnia?dl=0 https://www.dropbox.com/sh/g9ld1eqc31x2n1c/AABorTVBafzzpSPE4PvoXK94a?dl=0 https://www.dropbox.com/sh/d4pk0w3tbi6y8uk/AABv1qAErmWnlgY8EhXvLaZWa?dl=0 https://www.dropbox.com/sh/jvf4xk2azap3ptx/AABrcPXXrMECxSiWpbIy-Z3ca?dl=0

ISOFTBET

URL: https://partnerzone.isoftbet.com/ Username: sara.turk@igamingplatform.com Password: igamingplatform2019!

IRON DOG

URL: https://assets.1x2network.com/login.html Username: am@blueoceangaming.com Password: 1x2Blueocean!

KALAMBA

URL: https://www.kalambagames.com/operators-hub/ Password: KalambaOperator2016!

KIRON

URL: https://drive.google.com/drive/folders/0B8IfpDbms4DHSGUwNC1IUXJfMzQ](https://drive.google.com/drive/folders/0B8IfpDbms4DHSGUwNC1IUXJfMzQ

LEAP

URL: https://assets.1x2network.com/login.html Username: am@blueoceangaming.com Password: 1x2Blueocean!

LIVEG24

URL: https://www.liveg24.com/media/ pass: alfa2020

MASCOT GAMES

URL: https://drive.google.com/drive/folders/1G3b5iO2EFiyn0W8T993NgO-OBpFAeZtH

MERKUR

URL https://cloud.edictmaltaservices.com.mt/index.php/login Username: gamesolution Password: firetrap-smug-hawaii

URL: https://promotion.edict.de/en Username: gamesolution Password: firetrap-smug-hawaii

MICROGAMING (2 BY 2, JFTW, RABCAT, FOXIUM, BIG TIME GAMING, REAL DEALER STUDIO)

URL: www.microgaming.co.uk Username: sara.turk@blueoceangaming.com Password: MicrogamingJuly2020!

MR SLOTTY

URL: https://downloads.mrslotty.com/ Username: guest Password: 9MW9k0n9GfSyeTmn

NETENT

URL: https://clientarea.netent.com/ Username: notifications@igamingplatform.com Password: 83KjsubZhu&B8TBO

NOLIMIT

URL: https://www.nolimitcity.com/operator/ Username: sara.turk@igamingplatform.com Password: 83rX0U

NSOFT

URL: https://drive.google.com/drive/folders/1IklOfGBYDS_K-HFiLb-4F-tMf5dDklk5

ONEGAME

URL:https://owncloud.one-game.com/index.php/s/lZJwaza95ceGiIB Password: 1g2b0G

ONE TOUCH

URL: https://docs.google.com/spreadsheets/d/1p6hqUlysFrwcoAyqJhJls-jBg0roaK5BFvy29Mgb6zc/edit#gid=0 https://www.dropbox.com/sh/b1derx8f24zjlns/AAD3ud3ZmGzoq8Lr_juYgE79a?dl=0

ORYX GAMING (GAMOMAT, GOLDEN HERO, GIVME, PETER&SONS, ARCADEM, CANDLEBETS)

URL: https://oryxgaming.client-area.app/ Username: Sara Password: OryxNEWCA2022!

PLAYPEARLS

URL: https://stage.playpearls.com/integration/admin/en/login/ Username: igamingplatform Password: edFNshgEg7qdX5V4

PLATIPUS

URL: https://drive.slotegrator.com/s/V098QghhsS83I9H

PG SOFT

URL: https://www.pgsoft.com/download/

PLAYSON

URL: https://playson.com/client_area/ Username: Dejan.Jovic@blueoceangaming.com Password: yj5JJitH2l

PLAYTECH

URL: https://api.marketplace.playtech.com/marketplace/clientArea Username: iztok@blueoceangaming.com Password: W7Cv4SVbbVnEcBp*

Live casino URL: https://playtech-my.sharepoint.com/personal/yuliia_drozdova_playtech_com/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Fyuliia%5Fdrozdova%5Fplaytech%5Fcom%2FDocuments%2FLive%20Table%20Assets

PRAGMATIC PLAY

URL: http://clienthub.pragmaticplay.com/ Username: support@igamingplatform.com Password: W(,yrg"r8Y*p

PUSH GAMING

URL: https://clientarea.pushgaming.com/ Username: notifications@igamingplatform.com Password: PxRSzTf7K1MQ

QUICKSPIN

URL: https://clientarea.quickspin.com/wp/wp-login.php Username: sara.turk@blueoceangaming.com Password: h!UCaI89OJ3^BCBCMSY$jugu

REDRAKE GAMING

URL: https://bo.redrakegaming.com/ Username: BlueOcean Password: 64ezWxhyT6CAAvM

RED TIGER

URL: https://games.redtiger-demo.com/app/lobby Username: iGamingPlatform Password: S9ZxkEsg

REELPLAY

URL: reel-play.com/demo Password: $tack3dWi1d5

REEVO

URL: https://www.dropbox.com/sh/nun626s6gfo8cac/AAASgGqAA8U7cTR6A8MSJM95a?

RELAX GAMING (FANTASMA, BIG TIME GAMING)

URL: https://clientarea.relax-gaming.com/ Username: kristeena.haidon@igamingplatform.com Password: IGP2018

Will, [23-04-2022 10:07] REVOLVER

URL: https://demo.revolvergaming.com/ Username: client@revolvergaming.com Password: 4mdVbYQf

SALSA (EX PATAGONIA)

URL: https://drive.google.com/drive/folders/1V2pZr6AqnnnM4wwelWI2B2IcM3azaz5C

SLOTVISION

URL: https://slotvision.net/client-area/ Username: sara.turk@igamingplatform.com Password: !g3#eNa^1O1e!ue52L@1bScp

SPINOMENAL

URL: https://goo.gl/V69k88 Password: spinomenalguest

URL: https://ln4.sync.com/dl/98df47170/ps9cpzxs-i8xxg5cb-ktr683yz-5vt9f9dp Password: retroguest

SPINMATIC

URL: https://drive.google.com/drive/u/0/folders/1b2KjnRlY6voCUAs1UILSMaSQvoElqkja

SPADE GAMING

URL: https://www.dropbox.com/sh/fke24i4jfx3ewfw/AACpE-pvKXu6GaI-OiyKBvsra?dl=0 https://www.dropbox.com/sh/nysp9xisev95pv1/AAATO01c8ukktIpXyeY4_RVma?dl=0

SMARTSOFT GAMING

URL: https://www.dropbox.com/sh/e4ngfq8745h63vw/AABw_yK_NfR-fo6Oe7zmKmaga?dl=0

SWINTT

URL: https://vault.swintt.com Username: clientarea@blueoceangaming.com Password: BlueOcean2020!

SYNOT GAMING

URL: https://www.synotgames.com/download

STAKELOGIC

URL: https://ngb.gs-stakelogic.com/#/materials Username: sara.turk Password: Sarastake2022!#

THUNDERKICK

URL: https://www.thunderkick.com/resources/ Username: sara.turk Password: Sara2021

TOM HORN

URL: https://www.tomhorngaming.com/client-zone/ Username: Francesca Password: jWVIUwv45c!nF#Fj6LjsLzBT

TRIPLE PG

URL: https://client.triple-pg.com/documents

VIRTUAL GENERATION

URL: https://drive.google.com/drive/folders/1AjjdT7wHRQL4XavKvPfhBoiE7XGnXd8b

VIVO GAMING / VIVO ALADDIN

URL: http://assets.vivogaming.com Username: vivoclient Password: 6jeXLcYHIM

WAZDAN

URL: https://panel.wazdan.com/ Username: sara.turk@blueoceangaming.com Password: Sarawazdan2021!

WIZARD GAMES (EX PARIPLAY)

URL: http://www.pariplayltd.com/client-area/login/ Username: games@igamingplatform.com Password: IGAMINGPLATFORM

WOOHOO GAMING

NO client area. ZIP folders

YGGDRASIL

URL: https://source-beta.yggdrasilgaming.com/ Username: notifications@blueoceangaming.com Password: !hCX7*RrBNwObnJ$

```


